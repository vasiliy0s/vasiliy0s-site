# @vasiliy0s personal website

[![pipeline status](https://gitlab.com/vasiliy0s/vasiliy0s-site/badges/master/pipeline.svg)](https://gitlab.com/vasiliy0s/vasiliy0s-site/commits/master)

## Welcome

You are looking to [my](https://vasiliy0s.com) personal website sources.

It built with [CRA (Create React App)](https://github.com/facebook/create-react-app) tool fork with [TypeScript (react-scripts-ts)](https://github.com/wmonk/create-react-app-typescript) support.

Yes, _I love languages with strict typings for enterprise-level coding_

## Tech Stack

* [React 16](https://github.com/facebook/react/releases)
* [TypeScript](https://github.com/Microsoft/TypeScript-React-Starter#typescript-react-starter)
* [Create React App](https://github.com/facebook/create-react-app) (including [Webpack](https://webpack.js.org/)) + [react-scripts-ts](https://www.npmjs.com/package/react-scripts-ts)
* [Styled Components](https://www.styled-components.com/docs/api#typescript)
* [PolishedJS](https://polished.js.org/)
* [TSLint](https://palantir.github.io/tslint/) with [tslint-config-airbnb](https://www.npmjs.com/package/tslint-config-airbnb) preset

## Local run

If you would like (by any crazy reason) to run my app locally, just follow next steps:

```sh
git clone git@gitlab.com:vasiliy0s/vasiliy0s-site.git &&\
cd vasiliy0s-site &&\
npm ci &&\
npm start # then open http://localhost:3000 on browser
```

If you found an issue - just [report me here](https://gitlab.com/vasiliy0s/vasiliy0s-site/issues) or tell me in any direct message you would prefer (see [contacts](#contacts) section below).

## Contacts

All my actual contacts you can find on:

* actually [this website](https://vasiliy0s.com);
* on my [up-to-date CV](https://docs.google.com/document/d/1xUej7A3E2G1o1EJypjaHMJqlsiHMN4HhHdquZPwCarI/edit);
* if you want to offer me best-position-in-the-world or bit better, just let me know via [my LinkedIn profile](https://www.linkedin.com/in/vasiliy0s/).
