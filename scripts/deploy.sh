#!/usr/bin/env sh

set -e

BUILD_FOLDER="${1}"
DEPLOYMENT_HOST="${2}"
DEPLOYMENT_PORT="${3}"
DEPLOYMENT_PATH="/var/www/vasiliy0s.com"
BUILD_DIRECTORY=`date -u +%F--%T%z`

if [ -z "${BUILD_FOLDER}" ] || [ -z "${DEPLOYMENT_HOST}" ] || [ -z "${DEPLOYMENT_PORT}" ]; then
  echo "Some of arguments wasn't passed: BUILD_FOLDER, DEPLOYMENT_HOST, DEPLOYMENT_PORT"
  exit 1
fi

echo "Starting deployment from '${BUILD_FOLDER}' to ${DEPLOYMENT_HOST}..."

NEW_DEPLOYMENT_DIR="${DEPLOYMENT_PATH}/${BUILD_DIRECTORY}"

echo "Creates deployment folder..."
ssh ${DEPLOYMENT_HOST} -p ${DEPLOYMENT_PORT} "
  mkdir ${NEW_DEPLOYMENT_DIR}
"

echo "Copying current site to remote..."
scp -r -B -P ${DEPLOYMENT_PORT} ${BUILD_FOLDER}/* ${DEPLOYMENT_HOST}:${NEW_DEPLOYMENT_DIR}

echo "Switching to latest version..."
ssh ${DEPLOYMENT_HOST} -p ${DEPLOYMENT_PORT} "
  if [ -e ${DEPLOYMENT_PATH}/current ]; then
    unlink ${DEPLOYMENT_PATH}/current
  fi
  ln -s ./${BUILD_DIRECTORY} ${DEPLOYMENT_PATH}/current
"

echo "Done! Enjoy yourself"
