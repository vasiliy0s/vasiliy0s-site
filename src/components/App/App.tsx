import React from 'react';

import '@vasiliy0s-site/theme/global-styles';
import '@vasiliy0s-site/theme/normalize';

import { Layout } from '@vasiliy0s-site/components/Layout';
import { MainPage } from '@vasiliy0s-site/pages/MainPage';

export const App: React.StatelessComponent = () => (
  <Layout>
    <MainPage />
  </Layout>
);
