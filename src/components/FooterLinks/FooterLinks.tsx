import React from 'react';

import {
  FooterLinksContainer,
  FooterLinksContainerLayoutMode,
  ProtectedLink,
} from '@vasiliy0s-site/components/FooterLinks';
import { IMenuItem } from '@vasiliy0s-site/types';

interface IFooterLinksProps {
  links: IMenuItem[];
}

interface IFooterLinksState {
  layoutMode: FooterLinksContainerLayoutMode;
}

export class FooterLinks extends React.Component<IFooterLinksProps, IFooterLinksState> {
  public state: IFooterLinksState = {
    layoutMode: FooterLinksContainerLayoutMode.row,
  };

  private linkRefs: HTMLAnchorElement[] = [];
  private containerRef: HTMLDivElement;

  public componentDidMount() {
    window.addEventListener('resize', this.recomputeLinkWidths);
  }

  public componentWillUnmount() {
    window.addEventListener('resize', this.recomputeLinkWidths);
  }

  public render() {
    const { links } = this.props;
    const { layoutMode } = this.state;

    return (
      <div ref={this.handleLinksContainerRef}>
        <FooterLinksContainer className={layoutMode}>
          {links.map((link, index) => (
            <div
              key={link.href}
              className="footer-link-container"
            >
              <ProtectedLink
                className="footer-link"
                href={link.href}
                anchorRef={this.linkRectRef(index)}
                target="_blank"
              >
                {link.label}
              </ProtectedLink>
            </div>
          ))}
        </FooterLinksContainer>
      </div>
    );
  }

  private linkRectRef(index: number) {
    return (anchorRef: HTMLAnchorElement) => {
      this.linkRefs[index] = anchorRef;
      this.recomputeLinkWidths();
    };
  }

  private handleLinksContainerRef = (ref: HTMLDivElement) => {
    this.containerRef = ref;
    this.recomputeLinkWidths();
  }

  private recomputeLinkWidths = () => {
    this.linkRefs.length = this.props.links.length;
    if (!this.containerRef || !this.linkRectRef.length) {
      return;
    }

    const linksWidthSum = this.linkRefs.reduce(
      (aggr, ref) => aggr + ref.getBoundingClientRect().width,
      0,
    );
    const containerWidth = this.containerRef.getBoundingClientRect().width;

    this.setState({
      layoutMode: linksWidthSum > containerWidth ?
        FooterLinksContainerLayoutMode.column :
        FooterLinksContainerLayoutMode.row,
    });
  }
}
