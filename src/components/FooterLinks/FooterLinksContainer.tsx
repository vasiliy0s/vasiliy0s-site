import { styled } from '@vasiliy0s-site/theme';
import { border, rem, transparentize } from 'polished';

export enum FooterLinksContainerLayoutMode {
  row = 'layout-row',
  column = 'layout-column',
}

export const FooterLinksContainer = styled.footer`
  display: flex;
  justify-content: space-between;

  &.${FooterLinksContainerLayoutMode.row} {
    flex-direction: row;
  }

  &.${FooterLinksContainerLayoutMode.column} {
    flex-direction: column;
    background: ${({ theme }) => transparentize(0.2, theme.primaryColorInverted)};
    padding: ${rem('10px')};
  }

  .footer-link-container {
    padding: ${rem('5px')} ${rem('10px')};
  }

  .footer-link {
    font-size: 18px;

    &, &:hover, &:active, &:visited {
      color: ${({ theme }) => theme.primaryColor};
      text-decoration: none;
    }

    &:hover {
      ${({ theme }) => border('bottom', rem('2px'), 'solid', theme.primaryColor)}
    }
  }

`;
