import React from 'react';

interface IProtectedLinkProps {
  href: string;
  title?: string;
  children?: React.ReactNode;
  target?: string;
  className?: string;
  anchorRef?(ref: HTMLAnchorElement): void;
}

interface IProtectedLinkState {
  showLink?: boolean;
}

export class ProtectedLink extends React.Component<IProtectedLinkProps, IProtectedLinkState> {

  public state: IProtectedLinkState = {
    showLink: false,
  };

  private readonly DEFAULT_HREF_VALUE: string = '';

  public render() {
    const { children, title, className, target } = this.props;

    return (
      <a
        href={this.DEFAULT_HREF_VALUE}
        className={className}
        target={target}
        ref={this.handleRef}
        title={title}
      >
        {children}
      </a>
    );
  }

  private handleRef = (ref: HTMLAnchorElement) => {
    const setHref = () => ref.setAttribute('href', this.props.href);
    const unsetHref = () => ref.setAttribute('href', this.DEFAULT_HREF_VALUE);

    ref.addEventListener('mouseenter', setHref);
    ref.addEventListener('mouseleave', unsetHref);

    ref.addEventListener('focus', setHref);
    ref.addEventListener('blur', unsetHref);

    if (this.props.anchorRef) {
      this.props.anchorRef(ref);
    }
  }

}
