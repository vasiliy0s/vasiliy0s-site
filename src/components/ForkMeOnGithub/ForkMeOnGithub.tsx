import React from 'react';

import { ForkMeOnGithubLink } from '@vasiliy0s-site/components/ForkMeOnGithub';
const {
  ReactComponent: ForkMeOnGithubSvg,
// tslint:disable-next-line no-var-requires
} = require('@svgr/webpack?-prettier,-svgo!./ForkMeOnGithub.svg');

export interface IForkMeOnGithubProps {
  href: string;
  label: string;
}

export const ForkMeOnGithub: React.StatelessComponent<IForkMeOnGithubProps> = ({
  href,
  label,
}) => (
  <ForkMeOnGithubLink
    href={href}
    target="_blank"
    title={label}
    aria-label={label}>
      <ForkMeOnGithubSvg/>
  </ForkMeOnGithubLink>
);
