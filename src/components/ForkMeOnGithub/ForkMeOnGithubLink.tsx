import { styled } from '@vasiliy0s-site/theme';

export const ForkMeOnGithubLink = styled.a`
  position: absolute;
  top: 0;
  right: 0;
  width: 3rem;

  > svg {
    width: 100%;
    height: auto;
  }

  &:hover .octo-arm {
    animation: octocat-wave 560ms ease-in-out;
  }

  .octo-arm, .octo-body {
    fill: ${props => props.theme.primaryColorInverted};
  }

  .octo-triangle-bg {
    fill: ${props => props.theme.primaryColor};
  }

  @keyframes octocat-wave {
    0%, 100% {
      transform: rotate(0);
    }

    20%, 60% {
      transform: rotate(-25deg);
    }

    40%, 80% {
      transform: rotate(10deg);
    }
  }

  @media (max-width:500px) {
    :hover .octo-arm {
      animation: none;
    }

    & .octo-arm {
      animation: octocat-wave 560ms ease-in-out;
    }
  }
`;
