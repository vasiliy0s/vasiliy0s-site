import React from 'react';

import { ForkMeOnGithub } from '@vasiliy0s-site/components/ForkMeOnGithub';
import { LayoutContainer } from '@vasiliy0s-site/components/Layout';
import { siteTheme, ThemeProvider } from '@vasiliy0s-site/theme';

const {
  REACT_APP_FORK_ME_ON_GITHUB_LINK: FORK_ME_ON_GITHUB_LINK,
} = process.env;

interface ILayoutProps {
  children?: React.ReactNode;
}

export const Layout: React.StatelessComponent<ILayoutProps> = ({
  children,
}) => (
  <ThemeProvider theme={siteTheme}>
    <LayoutContainer>
      <ForkMeOnGithub
        href={`${FORK_ME_ON_GITHUB_LINK}`}
        label="Show sources on GitLab"
      />

      {children}
    </LayoutContainer>
  </ThemeProvider>
);
