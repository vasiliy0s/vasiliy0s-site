import { styled } from '@vasiliy0s-site/theme/styled-components';

export const LayoutContainer = styled.section`
  height: 100%;
  min-height: 100vh;
  max-width: 18000px; /* A really big width */
`;
