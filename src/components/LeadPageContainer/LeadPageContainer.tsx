import { styled } from '@vasiliy0s-site/theme';
import { rem, size } from 'polished';

import bgPng from './bg.png';

export const LeadPageContainer = styled.div`
  &, .lead-page-filter {
    ${size('100%', '100%')};
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  background-color: ${props => props.theme.primaryColorInverted};
  background-image: url(${() => bgPng});
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center;
  background-size: cover;

  color: ${({ theme }) => theme.primaryColor};

  &, .lead-page-filter {
    > .content, > .footer {
      width: 100%;
      padding: ${rem('30px')};
      box-sizing: border-box;
    }

    @media all and (max-width: ${props => props.theme.mediaBreakpoints.desktop}) {
      > .content {
        padding-bottom: 0;
      }
      > .header {
        margin-bottom: 2rem;
      }
    }

    > .content {
      flex: 1;
      display: flex;
      flex-direction: column;
      justify-content: center;
      max-width: ${({ theme }) => theme.maxPageWidth};
    }

    > .footer {
      flex: 0;
    }
  }
`;
