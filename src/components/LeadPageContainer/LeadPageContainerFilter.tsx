import { styled } from '@vasiliy0s-site/theme';

export const LeadPageContainerFilter = styled.div`
    height: 100%;
    width: 100%;
    background: transparent; /* fallback */
    background: rgba(0, 0, 0, 0.33);
`;
