import React from 'react';

import logoVasiliy0sPng from './logo-vasiliy0s.png';

export interface ILogoContainerProps {
  title?: string;
  alt?: string;
}

export const LogoContainer: React.StatelessComponent<ILogoContainerProps> = ({
  title,
  alt,
}) => (
    <div className="image-container">
      <img src={logoVasiliy0sPng} title={title} alt={alt} />
    </div>
  );
