import React from 'react';

import {
  LogoContainer,
  TriangleLogoContainer,
  TriangleLogoCorner,
  TriangleLogoLink,
} from '@vasiliy0s-site/components/TriangleLogo';

const {
  REACT_APP_MY_NICKNAME: MY_NICKNAME,
  REACT_APP_MY_NICKNAME_ALT_TEXT: MY_NICKNAME_ALT_TEXT,
} = process.env;

interface ITriangleLogoProps {
  href?: string;
  title?: string;
  alt?: string;
}

export const TriangleLogo: React.StatelessComponent<ITriangleLogoProps> = ({
  href = '/',
  alt = MY_NICKNAME_ALT_TEXT,
  title = MY_NICKNAME,
}) => (
  <TriangleLogoContainer>
    <TriangleLogoLink href={href} title={title}>
      <TriangleLogoCorner/>
      <LogoContainer title={title} alt={alt}/>
    </TriangleLogoLink>
  </TriangleLogoContainer>
);
