import { styled } from '@vasiliy0s-site/theme';

export const TriangleLogoContainer = styled.div`
  width: ${({ theme }) => theme.triangleLogoContainerWidth};

  left: 50%;

  .image-container {
    margin-top: 10%;
    position: relative;
    z-index: 1;

    img {
      width: 100%;
      height: auto;
    }
  }
`;
