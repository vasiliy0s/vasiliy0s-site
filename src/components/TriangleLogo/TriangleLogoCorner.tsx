import { styled } from '@vasiliy0s-site/theme';

const {
  ReactComponent: LogoBgTriangleSvg,
  // tslint:disable-next-line no-var-requires
} = require('@svgr/webpack?-prettier,-svgo!./logo-bg-triangle.svg');

export const TriangleLogoCorner = styled(LogoBgTriangleSvg)`
    fill: ${({ theme }) => theme.primaryColorInverted};
    position: absolute;
    left: 50%;
    transform: translateX(-50%) translateY(-26%);
    z-index: 1;
    width: 220px;
    height: auto;
`;
