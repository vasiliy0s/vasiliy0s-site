import { IMenuItem } from '@vasiliy0s-site/types';

const {
  REACT_APP_MY_EMAIL: MY_EMAIL,
  REACT_APP_MY_LINKED_IN_PROFILE_LINK: MY_LINKED_IN_PROFILE_LINK,
  REACT_APP_MY_CV_LINK: MY_CV_LINK,
  REACT_APP_MY_GITLAB_LINK: MY_GITLAB_LINK,
} = process.env;

export const footerMenuLinks: IMenuItem[] = [
  {
    href: `mailto:${MY_EMAIL || ''}`,
    label: '/email_me',
    title: 'Send me email',
  },
  {
    href: `${MY_LINKED_IN_PROFILE_LINK || ''}`,
    label: '/linked_in',
    title: 'See my profile on LinkedIn',
  },
  {
    href: `${MY_GITLAB_LINK || ''}`,
    label: '/gitlab',
    title: 'My public projects on GitLab',
  },
  {
    href: `${MY_CV_LINK || ''}`,
    label: '/cv',
    title: 'My skills in summary',
  },
];
