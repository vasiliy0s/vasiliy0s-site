import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { App } from '@vasiliy0s-site/components/App';
import registerServiceWorker from '@vasiliy0s-site/registerServiceWorker';

ReactDOM.render(
  <App/>,
  document.getElementById('root') as HTMLElement,
);
registerServiceWorker();
