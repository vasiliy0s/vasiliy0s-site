import React from 'react';

import { FooterLinks } from '@vasiliy0s-site/components/FooterLinks';
import {
  LeadPageContainer,
  LeadPageContainerFilter,
} from '@vasiliy0s-site/components/LeadPageContainer';

import { TriangleLogo } from '@vasiliy0s-site/components/TriangleLogo';
import { footerMenuLinks } from '@vasiliy0s-site/data';
import { MainPageContent, MainPageContentContainer } from '@vasiliy0s-site/pages/MainPage';

export const MainPage: React.StatelessComponent = () => (
  <LeadPageContainer>
    <LeadPageContainerFilter className="lead-page-filter">
      <header className="header">
        <TriangleLogo />
      </header>

      <div className="content">
        <MainPageContentContainer>
          <MainPageContent />
        </MainPageContentContainer>
      </div>

      <footer className="footer">
        <FooterLinks links={footerMenuLinks}/>
      </footer>
    </LeadPageContainerFilter>
  </LeadPageContainer>
);
