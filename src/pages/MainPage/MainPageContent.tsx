import React from 'react';

import avatarPng from './avatar.png';

export const MainPageContent: React.StatelessComponent = () => (
  <div className="main-page-content">
    <div className="main-page-content-text">
      <h2 className="main-page-content-my-name">
        Vasiliy "vasiliy0s" Teliatnikov
      </h2>

      <h1 className="main-page-content-my-title">
        Software Engineer
      </h1>

      <div className="main-page-content-blockquote">
        <blockquote>
          <em>
            “Truth can only be found
            <br className="hide-tablet"/>
            <span/> in one place: the code.”
          </em>
        </blockquote>

        <div className="main-page-content-blockquote-sign">
          Robert C. Martin
        </div>
      </div>
    </div>

    <div className="main-page-content-image">
      <img src={avatarPng}/>
    </div>
  </div>
);
