import { styled } from '@vasiliy0s-site/theme';
import { border, rem, transparentize } from 'polished';

export const MainPageContentContainer = styled.div`
  .main-page-content {
    display: flex;
  }

  .main-page-content-image > img {
    border-radius: 50%;
  }

  /* Table layout */
  @media all and (max-width: ${props => props.theme.mediaBreakpoints.desktop}) {
    font-size: ${rem('14px')};

    .hide-tablet {
      display: none;
    }

    .main-page-content {
      flex-direction: column-reverse;
    }

    .main-page-content-image {
      width: 50%;
      align-self: center;
      max-width: ${rem('150px')};

      > img {
        ${({ theme }) => border(
          rem('3px'),
          'solid',
          transparentize(1 - theme.primaryColorOpacity, theme.primaryColor),
        )};
        width: 100%;
      }
    }

    .main-page-content-my-name,
    .main-page-content-my-title {
      text-align: center;
    }

    .main-page-content-blockquote {
      margin-top: 1.5rem;

      blockquote {
        border-top: 1px solid;
        border-bottom: 1px solid;
        padding: 1rem 0.5rem;
        margin-left: 0;
        margin-right: 0;
      }

      .main-page-content-blockquote-sign {
        text-align: right;
      }
    }
  }

  /* Desktiop layout */
  @media all and (min-width: ${props => props.theme.mediaBreakpoints.desktop}) {
    font-size: ${rem('20px')};

    .main-page-content {
      flex-direction: row;
      justify-content: space-around;
      align-items: center;
    }

    .main-page-content-text {
      text-align: right;
      flex: 2;
      padding-bottom: 1rem;
      > h2 { margin-top: 0.83rem; }
    }

    .main-page-content-image {
      flex: 1;
      margin-left: 2rem;
      img {
        ${({ theme }) => border(
          rem('7px'),
          'solid',
          transparentize(1 - theme.primaryColorOpacity, theme.primaryColor),
        )};
        width: auto;
        height: ${({ theme }) => theme.avatarWidthHeight};
      }
    }

    .main-page-content-blockquote {
      margin-top: ${rem('40px')};
    }
  }
`;
