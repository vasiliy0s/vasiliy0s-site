import { injectGlobal, siteTheme } from '@vasiliy0s-site/theme';
import { rem } from 'polished';

// TODO: find way to switch theme here, when themes will be changable

// tslint:disable-next-line no-unused-expression
injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=PT+Mono');

  html {
    background-color: ${siteTheme.primaryColorInverted};
    font-family: 'PT Mono', monospace;
    font-size: ${rem('18px')};
  }

  body {
    min-height: 100%;
  }
`;
