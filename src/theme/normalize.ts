import { injectGlobal } from '@vasiliy0s-site/theme/styled-components';
import { normalize } from 'polished';

// tslint:disable-next-line no-unused-expression
injectGlobal`${normalize()}`;
