import { rem } from 'polished';

import { ITheme } from '@vasiliy0s-site/types';

export const siteTheme: ITheme = {
  avatarWidthHeight: rem('200px'),
  maxPageWidth: rem('1280px'),
  mediaBreakpoints: {
    desktop: '1024px',
    tablet: '768px',
  },
  primaryColor: '#57cece',
  primaryColorInverted: '#1c4242',
  primaryColorOpacity: 0.33,
  triangleLogoContainerWidth: rem('90px'),
};
