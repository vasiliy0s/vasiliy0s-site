import { ITheme } from '@vasiliy0s-site/types';
import * as styledComponents from 'styled-components';

const {
  default: styled,
  css,
  injectGlobal,
  keyframes,
  ThemeProvider,
} = styledComponents as styledComponents.ThemedStyledComponentsModule<ITheme>;

export {
  css,
  styled,
  injectGlobal,
  keyframes,
  ThemeProvider,
};
