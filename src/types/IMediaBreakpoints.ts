export interface IMediaBreakpoints {
  desktop: string;
  tablet: string;
}
