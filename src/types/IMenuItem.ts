export interface IMenuItem {
  label: string;
  title?: string;
  href: string;
}
