import { IMediaBreakpoints } from '@vasiliy0s-site/types';

export interface ITheme {
  maxPageWidth: string;

  primaryColor: string;
  primaryColorInverted: string;
  primaryColorOpacity: number;

  triangleLogoContainerWidth: string;

  avatarWidthHeight: string;

  mediaBreakpoints: IMediaBreakpoints;
}
